package main

import (
	"fmt"
	"portto/internal/crawler"
	"portto/internal/config"
	"portto/internal/router"
)

func main() {

	crawler.Run()

	r := router.Router()
	if err := r.Run(":"+config.ServicePort); err!=nil {
		fmt.Println(err.Error())
	}

}
