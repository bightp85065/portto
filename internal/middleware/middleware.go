package middleware

import (
	"github.com/gin-gonic/gin"
	"portto/internal/lib"
	"portto/internal/models"
	"strconv"
)

func CheckBlock() gin.HandlerFunc {
	return func(c *gin.Context) {

		blockIDStr := c.Param("block_id")
		blockID, err := strconv.ParseInt(blockIDStr, 10, 64)
		if err != nil {
			lib.ErrorResponse(c, err.Error(), 400)
			c.Abort()
			return
		}
		block := models.NewBlock()
		blockData, err := block.GetByID(blockID)
		if err!=nil {
			lib.ErrorResponse(c, err.Error(), 400)
			c.Abort()
			return
		}
		c.Set("block_data", blockData)

	}
}