package controllers

import (
    "github.com/gin-gonic/gin"
    "portto/internal/dbStruct"
    "portto/internal/lib"
)


type Transaction struct {}

func NewTransactionController() *Transaction {
    return &Transaction{}
}

func (t *Transaction) GetByTxHash(c *gin.Context) {

    txHash := c.Param("txHash")
    transaction, err := transactionModel.GetByTxHash(txHash)
    if err != nil {
        lib.ErrorResponse(c, "tx_hash isn't found", 400)
    }

    logs := transactionModel.GetLogByTxHash(txHash)

    logArr := make([]dbStruct.Log, 0)
    for _, l := range logs {
        logArr = append(logArr, dbStruct.Log{
            Index: l.LIndex,
            Data:  l.LData,
        })
    }
    c.JSON(200, dbStruct.ApiTransaction{
        TxHash:   transaction.TTxHash,
        From:     transaction.TFrom,
        To:       transaction.TTo,
        Nonce:    transaction.TNonce,
        Data:     transaction.TData,
        Value:    transaction.TValue,
        Logs:     logArr,
    })
}
