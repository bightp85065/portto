package controllers

import (
    "github.com/gin-gonic/gin"
    "portto/internal/dbStruct"
    "portto/internal/lib"
    "portto/internal/models"
    "strconv"
)

var (
    blockModel = models.NewBlock()
    transactionModel = models.NewTransaction()
)


type Block struct {}

func NewBlockController() *Block {
    return &Block{}
}

func (b *Block) GetBlockByID(c *gin.Context) {

    blockIf, _ := c.Get("block_data")
    body := blockIf.(dbStruct.Blocks)

    transactions := transactionModel.GetByBlock(*body.BlockNum)
    transactionsArr := make([]string, 0)
    for _, t := range transactions {
        transactionsArr = append(transactionsArr, *t.TTxHash)
    }
    c.JSON(200, dbStruct.ApiBlocksID{
        BlockNum:   body.BlockNum,
        BlockHash:   body.BlockHash,
        BlockTime:   body.BlockTime,
        ParentHash:   body.BlockParentHash,
        Transactions: transactionsArr,
    })
}

func (b *Block) GetNBlock(c *gin.Context) {
    limit := c.Query("limit")
    n, err := strconv.Atoi(limit)
    if err != nil {
        lib.ErrorResponse(c, "limit params err", 400)
    }
    if blockData, err := blockModel.GetLastN(n); err != nil {
        lib.ErrorResponse(c, err.Error(), 400)
    } else {
        c.JSON(200, blockData)
    }
}
