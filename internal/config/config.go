package config

// ConfigurationReadOnly can be implemented
// by Configuration, it's being used inside the Context.
// All methods that it contains should be "safe" to be called by the context
// at "serve time". A configuration field may be missing when it's not
// safe or its useless to be called from a request handler.

import (
)

var (
	ServicePort = "8080"
	DbPort = "3306"
	DbPwd = "123456"
    DbHost = "127.0.0.1"
	DbName = "portto"
	
	Start_scan_n int64 = 14015247 // 17129649
	RPCEndPoint = "https://data-seed-prebsc-2-s3.binance.org:8545/"
	Stable_n int64 = 20
)

func init() {

}
