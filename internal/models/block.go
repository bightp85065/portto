package models

import (
	"errors"
	"github.com/jinzhu/gorm"
	"portto/internal/dbStruct"
	"portto/internal/dbDriver"
	"portto/internal/config"
	"os"
)

var (
	db *gorm.DB
)

func init(){
	var err error
	db, err = dbDriver.DbInit(config.DbName)
	if err!=nil {
			os.Exit(1)
			return
	}
}

type Block struct {}

func NewBlock() *Block {
	m := &Block{}
	return m
}

func (b *Block) GetLastN(n int) (dbStruct.ApiBlocks, error){
	block := make([]dbStruct.Blocks, 0)
	db.Order("block_num desc").Limit(n).Find(&block)
	return dbStruct.ApiBlocks{
		Blocks: block,
	}, nil
}

func (b *Block) GetLastId() (int64){
	block := make([]dbStruct.Blocks, 0)
	db.Last(&block)
	if len(block) == 0 {
		return config.Start_scan_n
	}
	return *block[0].BlockNum
}

func (b *Block) GetByID(numID int64) (dbStruct.Blocks, error){
	block := dbStruct.Blocks{BlockNum:&numID}
	var count int
	db.Find(&block).Count(&count)
	if count==0 {
		return block, errors.New("id not exist")
	}
	return block, nil
}

func (b *Block) DbCreate(block dbStruct.Blocks) (dbStruct.Blocks, error){
	if err := db.Create(&block).Error; err!=nil {
		return dbStruct.Blocks{}, err
	}
	return block, nil
}

func (b *Block) DbUpdate(block dbStruct.Blocks) (dbStruct.Blocks, error) {

	ret := db.Model(&dbStruct.Blocks{BlockNum:block.BlockNum}).Update(block)
	if ret.Error!=nil {
		return dbStruct.Blocks{}, ret.Error
	}
	return b.GetByID(*block.BlockNum)
}
