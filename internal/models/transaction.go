package models

import (
	"errors"
	"portto/internal/dbStruct"
	"portto/internal/dbDriver"
	"portto/internal/config"
	"os"
)

func init(){
	var err error
	db, err = dbDriver.DbInit(config.DbName)
	if err!=nil {
		os.Exit(1)
		return
	}
}

type Transaction struct {}

func NewTransaction() *Transaction {
	t := &Transaction{}
	return t
}

func (t *Transaction) GetList() ([]dbStruct.Transactions, error){
	transactions := make([]dbStruct.Transactions, 0)
	db.Debug().Find(&transactions)
	return transactions, nil
}

func (t *Transaction) GetByBlock(blockNum int64) ([]dbStruct.Transactions){
	transactions := make([]dbStruct.Transactions, 0)
	db.Debug().Where(&dbStruct.Transactions{BlockNum:&blockNum}, "block_num").Find(&transactions)
	return transactions
}

func (t *Transaction) GetByTxHash(txHash string) (dbStruct.Transactions, error){
	transaction := dbStruct.Transactions{TTxHash:&txHash}
	var count int
	db.Find(&transaction).Count(&count)
	if count==0 {
		return transaction, errors.New("tx_hash isn't exist")
	}
	return transaction, nil
}

func (t *Transaction) GetLogByTxHash(txHash string) ([]dbStruct.Logs){
	logs := make([]dbStruct.Logs, 0)
	db.Debug().Where(&dbStruct.Logs{TTxHash:&txHash}, "t_tx_hash").Find(&logs)
	return logs
}

func (t *Transaction) DbCreate(transactions dbStruct.Transactions) (dbStruct.Transactions, error){
	if err := db.Debug().Create(&transactions).Error; err!=nil {
		return dbStruct.Transactions{}, err
	}

	return transactions, nil
}