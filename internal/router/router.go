package router

import (
	"github.com/gin-gonic/gin"
	"portto/internal/controllers"
	"portto/internal/middleware"
)

var (
	blockController = controllers.NewBlockController()
	transactionController = controllers.NewTransactionController()
)

func Router() *gin.Engine {

	r := gin.Default()

	blocks := r.Group("/blocks")
	{
		blocks.GET("", blockController.GetNBlock)
		blocksWithID := blocks.Group("/:block_id", middleware.CheckBlock())
		{
			blocksWithID.GET("", blockController.GetBlockByID)
		}
	}
	r.GET("/transaction/:txHash", transactionController.GetByTxHash)

	return r
}

