package dbDriver

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"portto/internal/config"
)

func DbInit(dbName string) (*gorm.DB, error){

	mysqlDbConn := fmt.Sprintf("root:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		config.DbPwd, config.DbHost, config.DbPort, dbName)
	var err error
	db, err := gorm.Open("mysql", mysqlDbConn)
	//defer db.Close()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	db.SingularTable(true)
	return db, nil
}
