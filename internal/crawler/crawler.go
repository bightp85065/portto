package crawler

// ConfigurationReadOnly can be implemented
// by Configuration, it's being used inside the Context.
// All methods that it contains should be "safe" to be called by the context
// at "serve time". A configuration field may be missing when it's not
// safe or its useless to be called from a request handler.

import (
	"context"
	"fmt"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/rpc"
	"math/big"
	"os"
	"portto/internal/config"
	"portto/internal/dbStruct"
	"portto/internal/models"
	"sync"
	"time"
)


var (
	blockModel *models.Block
	client *Client
	err error
	latest_num int64
	start_scan_n int64
	m *sync.Mutex
)

type Client struct {
	rpcClient *rpc.Client
	EthClient *ethclient.Client
}

func init() {

}

func Run() {
	
	fmt.Printf("try to connect %s", config.RPCEndPoint)
	client, err = Connect(config.RPCEndPoint)
	if err != nil {
		fmt.Println(err.Error())
		time.Sleep(5 * time.Second)
		os.Exit(1)
	}
	
	blockModel = models.NewBlock()
	start_scan_n = blockModel.GetLastId()
	fmt.Printf("GetLastId=%d\n", start_scan_n)

	m = new(sync.Mutex)
	go getLatest()
	for i:=1; i<=10; i++ {
		go crawlerBlock()
	}
	// find unstable to update
	
}

func Connect(host string) (*Client, error) {
	rpcClient, err := rpc.Dial(host)
	if err != nil {
		return nil, err
	}
	ethClient := ethclient.NewClient(rpcClient)
	return &Client{rpcClient, ethClient}, nil
}

func (ec *Client) GetBlockByNumberLatest(ctx context.Context) (*types.Header, error) {
	var result types.Header
	err := ec.rpcClient.CallContext(ctx, &result, "eth_getBlockByNumber", "latest", false)
	return &result, err
}

func getNextScanNumber() int64{
	m.Lock()
	start_scan_n++
	ret := start_scan_n
	m.Unlock()
	return ret
}

func crawlerBlock() {

	for {
		if latest_num == 0 {
			// wait for
			time.Sleep(1 * time.Second)
			continue
		}
		scanBlockNum := getNextScanNumber()

		stable := 1
		if latest_num - scanBlockNum < config.Stable_n {
			stable = 0
		}

		// stable and insert
		// if err, record id
		newBlockNumber := big.NewInt(scanBlockNum)
		block, err := client.EthClient.HeaderByNumber(context.TODO(), newBlockNumber)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		bNumber := block.Number.Int64()
		bHash := block.Hash().String()
		bTime := block.Time
		bParentHash := block.ParentHash.String()
		fmt.Printf("%d bNumber:%d, bHash:%s, time:%v, bParentHash:%v\n", 
			newBlockNumber, bNumber, bHash, bTime, bParentHash)
		
		if _, err = blockModel.GetByID(scanBlockNum); err == nil {
			fmt.Println("scanBlockNum data is exist")
			continue
		}
	
		blocks := dbStruct.Blocks {
			BlockNum: &bNumber,
			BlockHash: &bHash,
			BlockTime: &bTime,
			BlockParentHash: &bParentHash,
			BlockStable: &stable,
		}

		if _, err = blockModel.DbCreate(blocks); err != nil {
			fmt.Println(err.Error())
			continue
		}

		tx, _, err := client.EthClient.TransactionByHash(context.TODO(), block.Hash())
		if err != nil {
			fmt.Println(err.Error())
			time.Sleep(10 * time.Second)
			continue
		}
		
		txHash := tx.Hash().String()
		chainID, err := client.EthClient.NetworkID(context.Background())
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		
		msg, err := tx.AsMessage(types.NewEIP155Signer(chainID), nil)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		fmt.Printf("bHash=%s, txHash=%s, from=%s, to=%s nonce=%d, data=%s, value=%d", 
			bHash, txHash, msg.From().String(), msg.To().String(), msg.Nonce(), msg.Data(), msg.Value().Int64())
		// pseudo code
		// 1. insert by transactionModel.update


		//

	}
}

func getLatest() {
	for {
		lblock, err := client.GetBlockByNumberLatest(context.TODO())
		if err != nil {
			fmt.Println(err.Error()+" 3ERR")
		} else {
			latest_num = lblock.Number.Int64()
			fmt.Printf("latest number:%d\n", latest_num)
		}
		time.Sleep(5 * time.Second)
	}
}