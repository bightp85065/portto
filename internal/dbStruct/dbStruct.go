package dbStruct

type ApiBlocks struct {
	Blocks []Blocks `json:"blocks"`
}

type ApiBlocksID struct {
	BlockNum    *int64 `json:"block_num"`
	BlockHash   *string `json:"block_hash"`
	BlockTime   *uint64 `json:"block_time"`
	ParentHash  *string `json:"parent_hash"`
	Transactions []string `json:"transactions"`
}

type ApiTransaction struct {
	TxHash    *string `json:"tx_hash"`
	From      *string `json:"from"`
	To        *string `json:"to"`
	Nonce     *uint64 `json:"nonce"`
	Data      *string `json:"data"`
	Value     *int64  `json:"value"`
	Logs      []Log   `json:"logs"`
}

type Log struct {
	Index    *int `json:"index"`
	Data     *string `json:"data"`
}

type Blocks struct {
	BlockNum    *int64 `gorm:"primary_key" json:"block_num"`
	BlockHash   *string `gorm:"default:''" json:"block_hash"`
	BlockTime   *uint64 `gorm:"default:''" json:"block_time"`
	BlockParentHash   *string `gorm:"default:''" json:"block_parent_hash"`
	BlockStable   *int `gorm:"default:''" json:"block_stable"`
}

type Transactions struct {
	TTxHash    *string `gorm:"primary_key" json:"t_tx_hash"`
	TFrom      *string `gorm:"default:''" json:"t_from"`
	TTo        *string `gorm:"default:''" json:"t_to"`
	TNonce     *uint64 `gorm:"default:''" json:"t_nonce"`
	TData      *string `gorm:"default:''" json:"t_data"`
	TValue     *int64  `gorm:"default:''" json:"t_value"`
	BlockNum   *int64 `gorm:"default:''" json:"block_num"`
}

type Logs struct {
	LId       *int `gorm:"primary_key" json:"l_id"`
	LIndex    *int `gorm:"primary_key" json:"l_index"`
	LData     *string `gorm:"default:''" json:"l_data"`
	TTxHash   *string `gorm:"default:''" json:"t_tx_hash"`
}
