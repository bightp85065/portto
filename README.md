## Description

Backend interview assignment

## Prerequisites

* Golang 1.13 up
* Docker
* Make

## Spec

1. HTTP for port 8080, MYSQL for port 3306

## How to

* Build MYSQL

```
make run_docker_mysql
import scripts/init.sql
```

* Build Service

```
make portto
```


## Known issue

1. method: eth_gettransactionbyhash always "not found"

## TODO list

1. goroutine for finding unstable to update

2. Error table and goroutine for crawler failure