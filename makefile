SHELL=/bin/bash
name=test
tag=test

all: build_docker_image run_docker_mysql run_docker_image
portto: build_docker_image run_docker_image
portto_base: build_docker_image_from_base run_docker_image

build_base:
	@echo Start build base image
	docker build -t=portto_base/$(name):$(tag) --network host -f build/package/Dockerfile_base .

build_docker_image:
	@echo Start build image
	docker build -t=portto/$(name):$(tag) --network host -f build/package/Dockerfile .
	docker image prune --filter label=stage=build -f

build_docker_image_from_base:
	@echo Start build image
	docker build -t=portto_base/$(name):$(tag) --network host -f build/package/Dockerfile_from_base .
	# docker image prune --filter label=stage=build -f

run_docker_image:
	@echo Run docker image
	docker rm -f portto
	docker run -it -p 8080:8080 --name portto portto/$(name):$(tag) bash

run_docker_mysql:
	@echo Run docker mysql
	docker run -it -p 3306:3306 --name mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql