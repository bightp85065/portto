DROP DATABASE IF EXISTS portto;
CREATE database portto;
USE portto;

DROP TABLE IF EXISTS log;
DROP TABLE IF EXISTS transactions;
DROP TABLE IF EXISTS blocks;
-- Noticed:
-- `notallowed` field contains the string list that should be blocked in URL request. If the 
-- value contains `#`, the following string can be ignored for not being blocked if it exists 
-- in URL request. For example, `resource#apps` represents that all resource API should be 
-- blocked except for the URL which contains `apps` string.
CREATE TABLE blocks (
    block_num BIGINT,
    block_hash VARCHAR(255),
    block_time int UNSIGNED,
    block_parent_hash VARCHAR(255),
    block_stable int(1),
    PRIMARY KEY (block_num),
    INDEX (block_stable)
);

CREATE TABLE transactions (
    t_tx_hash   VARCHAR(32),
    t_from      VARCHAR(255),
    t_to        VARCHAR(255),
    t_nonce     BIGINT UNSIGNED,
    t_data      VARCHAR(255),
    t_value     BIGINT,
    block_num   BIGINT,
    FOREIGN KEY (block_num) REFERENCES blocks(block_num),
    PRIMARY KEY (t_tx_hash)
);

CREATE TABLE logs (
    l_id       Int(11) NOT NULL AUTO_INCREMENT,
    l_index    Int(11),
    l_data     VARCHAR(255),
    t_tx_hash  VARCHAR(32),
    FOREIGN KEY (t_tx_hash) REFERENCES transactions(t_tx_hash),
    PRIMARY KEY (l_id)
);
